import React from 'react';
import HttpService from '../../services/http';

const http = new HttpService();
export default class NavComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFilm: this.props.selectedFilm,
            films: [],
            posterUrl: ''
        };
    }

    async componentDidMount() {
        const data = await http.getData('https://swapi.dev/api/films/');
        const films = data.results.map((x, i) => {
            return {
                id: i + 1,
                ...x
            };
        });
        this.setState({
            films: films.sort((a, b) => a.episode_id > b.episode_id ? 1 : -1)
        });
    }

    componentDidUpdate(prevProps) {
        if (this.props.selectedFilm !== prevProps.selectedFilm) {
            this.setState(() => ({
                selectedFilm: this.props.selectedFilm
            }));
        }
    }

    handleClick(key) {
        this.props.onClick(key);
    }

    render() {
        return (
            <div>
                <div className='nav'>
                    {this.state.films.map((x, i) => {
                        return (
                            <div key={x.id} className={this.props.selectedFilm === x.id ? 'active' : ''} onClick={this.handleClick.bind(this, x.id)}>
                                <span>{x.title}</span>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }
}
