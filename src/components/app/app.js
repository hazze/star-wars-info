import { Component } from 'react';
import NavComponent from '../nav/nav';
import FilmComponent from '../film/film';
import logo from './../../assets/star_wars_logo.png';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFilm: 4
    }
  }

  handleFilmClick = (key) => {
    this.setState({
      selectedFilm: key
    });
  }

  render() {
    return (
      <div className="app">
        <div className="logo">
          <img src={logo} alt="Star Wars logo" height="125px" />
        </div>
        <NavComponent onClick={this.handleFilmClick} selectedFilm={this.state.selectedFilm} />
        <div className="wrapper">
          <FilmComponent selectedFilm={this.state.selectedFilm} />
        </div>
      </div>
    );
  }
}

export default App;
