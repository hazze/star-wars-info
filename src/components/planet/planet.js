import React from 'react';
import HttpService from './../../services/http';
import { BsFillPlusSquareFill, BsFillDashSquareFill } from 'react-icons/bs';

const http = new HttpService();

export default class PlanetComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.planetId,
            name: '',
            rotationPeriod: '',
            orbitalPeriod: '',
            diameter: '',
            climate: '',
            gravity: '',
            terrain: '',
            surfaceWater: '',
            population: '',
            displayDetails: false
        };

        this.handleOnClick = this.handleOnClick.bind(this);
    }

    async componentDidMount() {
        const data = await http.getData('https://swapi.dev/api/planets/' + this.state.id);
        this.setState({
            name: data.name,
            rotationPeriod: data.rotation_period,
            orbitalPeriod: data.orbital_period,
            diameter: data.diameter,
            climate: data.climate,
            gravity: data.gravity,
            terrain: data.terrain,
            surfaceWater: data.surface_water,
            population: data.population
        });
    }

    handleOnClick() {
        this.setState({
            displayDetails: !this.state.displayDetails
        });
    }

    render() {
        if (this.state.name) {
            if (this.state.displayDetails) {
                return (
                    <div className='content-details'>
                        <div className='icon'>
                            <BsFillDashSquareFill onClick={this.handleOnClick} />
                        </div>
                        <div>{this.state.name}</div>
                        <div></div>
                        <div className='details'>
                            <ul>
                                <li>
                                    <div className='title'>Population</div>
                                    <div className='value'>{this.state.population}</div>
                                </li>
                                <li>
                                    <div className='title'>Diameter</div>
                                    <div className='value'>{this.state.diameter} <span className='unit'>km</span></div>
                                </li>
                                <li>
                                    <div className='title'>Climate</div>
                                    <div className='value'>{this.state.climate}</div>
                                </li>
                                <li>
                                    <div className='title'>Gravity</div>
                                    <div className='value'>{this.state.gravity}</div>
                                </li>
                                <li>
                                    <div className='title'>Terrain</div>
                                    <div className='value'>{this.state.terrain}</div>
                                </li>
                                <li>
                                    <div className='title'>Surface water</div>
                                    <div className='value'>{this.state.surfaceWater}</div>
                                </li>
                                <li>
                                    <div className='title'>Rotation period</div>
                                    <div className='value'>{this.state.rotationPeriod}</div>
                                </li>
                                <li>
                                    <div className='title'>Orbital period</div>
                                    <div className='value'>{this.state.orbitalPeriod}</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                );
            }
            return (
                <div className='content-details'>
                    <div className='icon'>
                        <BsFillPlusSquareFill onClick={this.handleOnClick} />
                    </div>
                    <div>{this.state.name}</div>
                </div>
            );
        }
        return null;
    }
}
