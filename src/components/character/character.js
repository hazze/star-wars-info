import React from 'react';
import HttpService from './../../services/http';
import { BsFillPlusSquareFill, BsFillDashSquareFill } from 'react-icons/bs';

const http = new HttpService();
export default class CharacterComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.characterId,
            name: '',
            height: '',
            mass: '',
            hairColor: '',
            skinColor: '',
            eyeColor: '',
            brith: '',
            gender: '',
            displayDetails: false
        };

        this.handleOnClick = this.handleOnClick.bind(this);
    }

    async componentDidMount() {
        const data = await http.getData('https://swapi.dev/api/people/' + this.state.id);
        this.setState({
            name: data.name,
            height: data.height,
            mass: data.mass,
            hairColor: data.hair_color,
            skinColor: data.skin_color,
            eyeColor: data.eye_color,
            brith: data.birth,
            gender: data.gender
        });
    }

    handleOnClick() {
        this.setState({
            displayDetails: !this.state.displayDetails
        });
    }

    render() {
        if (this.state.name) {
            if (this.state.displayDetails) {
                return (
                    <div className='content-details'>
                        <div className='icon'>
                            <BsFillDashSquareFill onClick={this.handleOnClick} />
                        </div>
                        <div>{this.state.name}</div>
                        <div></div>
                        <div className='details'>
                            <ul>
                                <li>
                                    <div className='title'>Height</div>
                                    <div className='value'>{this.state.height} <span className='unit'>cm</span></div>
                                </li>
                                <li>
                                    <div className='title'>Mass</div>
                                    <div className='value'>{this.state.mass} <span className='unit'>cm</span></div>
                                </li>
                                <li>
                                    <div className='title'>Birth</div>
                                    <div className='value'>{this.state.brith}</div>
                                </li>
                                <li>
                                    <div className='title'>Gender</div>
                                    <div className='value'>{this.state.gender}</div>
                                </li>
                                <li>
                                    <div className='title'>Hair color</div>
                                    <div className='value'>{this.state.hairColor}</div>
                                </li>
                                <li>
                                    <div className='title'>Skin color</div>
                                    <div className='value'>{this.state.skinColor}</div>
                                </li>
                                <li>
                                    <div className='title'>Eye color</div>
                                    <div className='value'>{this.state.eyeColor}</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                );
            }
            return (
                <div className='content-details'>
                    <div className='icon'>
                        <BsFillPlusSquareFill onClick={this.handleOnClick} />
                    </div>
                    <div>{this.state.name}</div>
                </div>
            );
        }
        return null;
    }
}
