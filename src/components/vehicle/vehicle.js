import React from 'react';
import HttpService from '../../services/http';
import { BsFillPlusSquareFill, BsFillDashSquareFill } from 'react-icons/bs';

const http = new HttpService();
export default class VehicleComponent extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            id: this.props.starshipId,
            name: '',
            model: '',
            vehicleClass: '',
            length: '',
            crew: '',
            passengers: '',
            cargoCapacity: '',
            manufacturer: '',
            maxAtmospheringSpeed: '',
            consumables: '',
            costInCredits: '',
            displayDetails: false
        };

        this.handleOnClick = this.handleOnClick.bind(this);
    }

    async componentDidMount() {
        const data = await http.getData('https://swapi.dev/api/vehicles/' + this.state.id);
        this.setState({
            name: data.name,
            model: data.model,
            vehicleClass: data.vehicle_class,
            length: data.length,
            crew: data.crew,
            passengers: data.passengers,
            cargoCapacity: data.cargo_capacity,
            manufacturer: data.manufacturer,
            maxAtmospheringSpeed: data.max_atmosphering_speed,
            consumables: data.consumables,
            costInCredits: data.cost_in_credits
        });
    }

    handleOnClick() {
        this.setState({
            displayDetails: !this.state.displayDetails
        });
    }

    render() {
        if (this.state.name) {
            if (this.state.displayDetails) {
                return (
                    <div className='content-details'>
                        <div className='icon'>
                            <BsFillDashSquareFill onClick={this.handleOnClick} />
                        </div>
                        <div>{this.state.name}</div>
                        <div></div>
                        <div className='details'>
                            <ul>
                                <li>
                                    <div className='title'>Model</div>
                                    <div className='value'>{this.state.model}</div>
                                </li>
                                <li>
                                    <div className='title'>Vehicle class</div>
                                    <div className='value'>{this.state.vehicleClass}</div>
                                </li>
                                <li>
                                    <div className='title'>Length</div>
                                    <div className='value'>{this.state.length} <span className='unit'>m</span></div>
                                </li>
                                <li>
                                    <div className='title'>Crew</div>
                                    <div className='value'>{this.state.crew}</div>
                                </li>
                                <li>
                                    <div className='title'>Passengers</div>
                                    <div className='value'>{this.state.passengers}</div>
                                </li>
                                <li>
                                    <div className='title'>Cargo capacity</div>
                                    <div className='value'>{this.state.cargoCapacity}</div>
                                </li>
                                <li>
                                    <div className='title'>Manufacturer</div>
                                    <div className='value'>{this.state.manufacturer}</div>
                                </li>
                                <li>
                                    <div className='title'>Max atmosphering speed</div>
                                    <div className='value'>{this.state.maxAtmospheringSpeed} {this.state.maxAtmospheringSpeed !== 'unknown' ? (<span className='unit'>kph</span>) : ('') }</div>
                                </li>
                                <li>
                                    <div className='title'>Consumables</div>
                                    <div className='value'>{this.state.consumables}</div>
                                </li>
                                <li>
                                    <div className='title'>Cost</div>
                                    <div className='value'>{this.state.costInCredits} {this.state.costInCredits !== 'unknown' ? (<span className='unit'>credits</span>) : ('') }</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                );
            }
            return (
                <div className='content-details'>
                    <div className='icon'>
                        <BsFillPlusSquareFill onClick={this.handleOnClick} />
                    </div>
                    <div>{this.state.name}</div>
                </div>
            );
        }
        return null;
    }
}
