import React from 'react';
import HttpService from './../../services/http';
import CharacterComponent from './../character/character';
import PlanetComponent from './../planet/planet';
import StarshipComponent from './../starship/starship';
import VehicleComponent from './../vehicle/vehicle';

import episode1 from './../../assets/posters/episode_1.jpeg';
import episode2 from './../../assets/posters/episode_2.jpeg';
import episode3 from './../../assets/posters/episode_3.jpeg';
import episode4 from './../../assets/posters/episode_4.jpeg';
import episode5 from './../../assets/posters/episode_5.jpeg';
import episode6 from './../../assets/posters/episode_6.jpeg';

const http = new HttpService();
export default class FilmComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFilm: this.props.selectedFilm,
            id: 0,
            title: '',
            episodeId: 0,
            openingCrawl: '',
            director: '',
            producer: '',
            releaseDate: '',
            characters: [],
            planets: [],
            starships: [],
            vehicles: []
        };
    }

    componentDidMount() {
        this.getFilm(this.props.selectedFilm);
    }

    componentDidUpdate(prevProps) {
        if (this.props.selectedFilm !== prevProps.selectedFilm) {
            this.setState(() => ({
                id: this.props.selectedFilm
            }));
            this.getFilm(this.props.selectedFilm);
        }
    }

    async getFilm(id) {
        if (id > 0) {
            const data = await http.getData('https://swapi.dev/api/films/' + id);
            this.setState({
                id: id,
                title: data.title,
                episodeId: data.episode_id,
                openingCrawl: data.opening_crawl,
                director: data.director,
                producer: data.producer,
                releaseDate: data.release_date,
                characters: data.characters.splice(0, 15).map(x => x.match(/\d+/).shift()),
                planets: data.planets.map(x => x.match(/\d+/).shift()),
                starships: data.starships.map(x => x.match(/\d+/).shift()),
                vehicles: data.vehicles.map(x => x.match(/\d+/).shift())
            });
        } else {
            this.setState({
                title: '',
                episodeId: 0,
                openingCrawl: '',
                director: '',
                producer: '',
                releaseDate: ''
            });
        }
    }

    getPosterUrl() {
        switch (this.state.title) {
        case 'The Phantom Menace':
            return episode1;
        case 'Attack of the Clones':
            return episode2;
        case 'Revenge of the Sith':
            return episode3;
        case 'A New Hope':
            return episode4;
        case 'The Empire Strikes Back':
            return episode5;
        case 'Return of the Jedi':
            return episode6;
        default:
        }
    }

    render() {
        if (this.state.id) {
            return (
                <div className=''>
                    <div className='film-info'>
                        <div className='meta'>
                            <div>
                                <img src={this.getPosterUrl()} alt='Movie poster' width='200' />
                            </div>
                            <div className='createdBy'>
                                <div>Released {this.state.releaseDate}</div>
                                <div>Directed by {this.state.director}</div>
                                <div>Produced by {this.state.producer}</div>
                            </div>
                        </div>
                        <div className='description'>
                            <h2>{this.state.title}<span className='releaseDate'>{this.state.releaseDate}</span></h2>
                            <div className='opening'>
                                {this.state.openingCrawl}
                            </div>
                            <div className='content'>
                                <div>
                                    <h3>Characters</h3>
                                    {this.state.characters.map(x => {
                                        return <CharacterComponent key={'character-' + x} characterId={x} />;
                                    })}
                                </div>
                                <div>
                                    <h3>Planets</h3>
                                    {this.state.planets.map(x => {
                                        return <PlanetComponent key={'planet-' + x} planetId={x} />;
                                    })}
                                </div>
                                <div>
                                    <h3>Starships</h3>
                                    {this.state.starships.map(x => {
                                        return <StarshipComponent key={'ship-' + x} starshipId={x} />;
                                    })}
                                </div>
                                <div>
                                    <h3>Vehicles</h3>
                                    {this.state.vehicles.map(x => {
                                        return <VehicleComponent key={'ship-' + x} starshipId={x} />;
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        return (
            <div>
                No movie selected
            </div>
        );
    }
}
