export default class HttpService {
    constructor() {
        this.data = null;
        this.getData = this.getData.bind(this);
    }

    async getData(url) {
        await fetch(url)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Error when fetching data.');
                }
                return response.json();
            })
            .then(data => { this.data = data; })
            .catch(error => console.log(error));

        return this.data;
    }
}
